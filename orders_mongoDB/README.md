This folder establish the pipeline from:
database: kiwi_main
collection: orders

### orders_database_production:
This notebook points the optimized notebook for migrating updates from MongoDB database to aws S3.

### orders_database_setup:
Notebook for establishing the entire database from mongoDB. This notebook was only used once.