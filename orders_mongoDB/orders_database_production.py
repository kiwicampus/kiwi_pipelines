# Databricks notebook source
import os
from pyspark.sql.types import *
import pyspark
import json

#Get mongoDB data server ready
source = 'mongodb://%s:%s@ds155785-a1.mlab.com:55785/kiwi_main?replicaSet=rs-ds155785' % (os.environ.get("MONGODB_USER"), os.environ.get("MONGODB_PASSWORD"))

#Access s3 bucket
ACCESS_KEY = os.environ.get("s3_o_ACCESS_KEY")
SECRET_KEY = os.environ.get("s3_o_SECRET_KEY")
ENCODED_SECRET_KEY = SECRET_KEY.replace("/", "%2F")
AWS_BUCKET_NAME = "centralized-datasets-data-team"
MOUNT_NAME = "centdatasets"
#dbutils.fs.mount("s3a://%s:%s@%s" % (ACCESS_KEY, ENCODED_SECRET_KEY, AWS_BUCKET_NAME), "/mnt/%s" % MOUNT_NAME)

# COMMAND ----------

from datetime import datetime
timestamp = (datetime.timestamp(datetime.now()) - 1600000)*1000
timestamp

# COMMAND ----------

with open("/dbfs/foobar/schema_orders.json", "r") as f:
  data = json.load(f)
schema = StructType.fromJson(json.loads(data))
df = spark.read.format("mongo").option("spark.mongodb.input.uri", source).option("database", "kiwi_main").option("collection", "orders").option("batchSize", 1000).load(schema=schema)
df = df.filter(df['state.openTimestamp'] >= timestamp)
df.createOrReplaceTempView('__orders')

# COMMAND ----------

# MAGIC %sql
# MAGIC CREATE TEMPORARY VIEW orders AS
# MAGIC SELECT 
# MAGIC _id.oid, 
# MAGIC couponId,
# MAGIC botName,
# MAGIC clientId,
# MAGIC clientPrice,
# MAGIC client.braintreeCustomerId as braintreeCustomerId,
# MAGIC client.createdAt as clientcreatedAt,
# MAGIC client.customerId as clientcustomerId,
# MAGIC client.deleted as clientdeleted,
# MAGIC client.email as clientemail,
# MAGIC client.externalService as clientexternalService,
# MAGIC client.facebookId as clientfacebookId,
# MAGIC client.firebase_uid as clientfirebase_uid,
# MAGIC client.firstName as clientfirstName,
# MAGIC client.image as clientimage,
# MAGIC client.lastName as clientlastName,
# MAGIC client.name as clientname,
# MAGIC client.phoneNumber as clientphoneNumber,
# MAGIC client.prime as clientprime,
# MAGIC client.primeEnd as clientprimeEnd,
# MAGIC client.primeRenewable as clientprimeRenewable,
# MAGIC client.primeStart as clientprimeStart,
# MAGIC client.promoCode as clientpromoCode,
# MAGIC client.score as clientscore,
# MAGIC client.testCustomerId as clienttestCustomerId,
# MAGIC client.token as clienttoken,
# MAGIC client.zoneId as clientzoneId,
# MAGIC coupon.active as couponactive,
# MAGIC coupon.appliedAtCheckout as couponappliedatcheckout,
# MAGIC coupon.claimed as couponclaimed,
# MAGIC coupon.coupon as couponcoupon,
# MAGIC coupon.createdBy as couponcreatedby,
# MAGIC coupon.deleted as coupondeleted,
# MAGIC coupon.description as coupondescription,
# MAGIC coupon.endTimestamp as couponendTimestamp,
# MAGIC coupon.homeImage as couponhomeImage,
# MAGIC coupon.image as couponimage,
# MAGIC coupon.menuItemId as couponmenuItemId,
# MAGIC coupon.name as couponname,
# MAGIC coupon.orderEndTimestamp as couponorderEndTimestamp,
# MAGIC coupon.orderStartTimestamp as couponorderStartTimestamp,
# MAGIC coupon.restaurantId as couponrestaurantId,
# MAGIC coupon.showEndTimestamp as couponshowEndTimestamp,
# MAGIC coupon.showStartTimestamp as couponshowStartTimestamp,
# MAGIC coupon.startTimestamp as couponstartTimestamp,
# MAGIC coupon.units as couponunits,
# MAGIC coupon.value as couponvalue,
# MAGIC coupon.zoneId as couponzoneId,
# MAGIC comments,
# MAGIC description,
# MAGIC externalOrder,
# MAGIC fee,
# MAGIC firstTime,
# MAGIC holder.location.deleted as holderlocationdeleted,
# MAGIC holder.location.description as holderlocationdescription,
# MAGIC holder.location.lat as holderlocationlat,
# MAGIC holder.location.lng as holderlocationlng,
# MAGIC holder.type as holdertype,
# MAGIC isFirstTime,
# MAGIC kiwerUsername,
# MAGIC location.altitude as locationaltitude,
# MAGIC location.city as locationcity,
# MAGIC location.deleted as locationdeleted,
# MAGIC location.comments as locationcomments,
# MAGIC location.description as locationdescription,
# MAGIC location.lat as locationlat,
# MAGIC location.lng as locationlng,
# MAGIC location.precision as locationprecisio,
# MAGIC location.state as locationState,
# MAGIC location.street as locationstreet,
# MAGIC location.zipCode as locationzipCode,
# MAGIC messages,
# MAGIC modifiers,
# MAGIC numericId,
# MAGIC orderItems.clientPrice as orderItemsclientPrice,
# MAGIC orderItems.customizations as orderItemscustomizations,
# MAGIC orderItems.deleted as orderItemsdeleted,
# MAGIC orderItems.menuItem._Id as orderItemsmenuItem_Id,
# MAGIC orderItems.menuItem.active as orderItemsactive,
# MAGIC orderItems.menuItem.appActive as orderItemmenuItemappActive,
# MAGIC orderItems.menuItem.clientPrice as orderItemsmenuItemclientPrice,
# MAGIC orderItems.menuItem.deleted as orderItemsmenuItemdeleted,
# MAGIC orderItems.menuItem.description as orderItemsmenuItemdescription,
# MAGIC orderItems.menuItem.fbActive as orderItemsmenuItemfbActive,
# MAGIC orderItems.menuItem.image as orderItemsmenuItemimage,
# MAGIC orderItems.menuItem.markup as orderItemsmenuItemmarkup,
# MAGIC orderItems.menuItem.name as orderItemsmenuItemname,
# MAGIC orderItems.menuItem.originalPrice as orderItemsmenuItemoriginalPrice,
# MAGIC orderItems.menuItem.price as orderItemsmenuItemprice,
# MAGIC payment,
# MAGIC paymentMethod,
# MAGIC perk.date as perkdate,
# MAGIC perk.deleted as perkdeleted,
# MAGIC perk.perk as perkperk,
# MAGIC perk.phoneNumber as perkphoneNumber,
# MAGIC perk.priority as perkpriority,
# MAGIC perk.timestamp as perktimestamp, 
# MAGIC perk.used as perkused,
# MAGIC perk.userId as perkuserId,
# MAGIC perk.promoCode.active as perkpromoCodeactive,
# MAGIC perk.promoCode.batchIdentificator as perkpromoCodebatchIdentificator,
# MAGIC perk.promoCode.code as perkpromoCodecode,
# MAGIC pickupAddress.city as pickupAddressCity,
# MAGIC pickupAddress.comments as pickupAddressComments,
# MAGIC pickupAddress.deleted as pickupAddressDeleted,
# MAGIC pickupAddress.description as pickupAddressDescription,
# MAGIC pickupAddress.lat as pickupAddressLat,
# MAGIC pickupAddress.lng as pickupAddressLng,
# MAGIC pickupAddress.state as pickupAddressState,
# MAGIC pickupAddress.street as pickupAddressStreet,
# MAGIC pickupAddress.zipCode as pickupAddressZipCode,
# MAGIC price,
# MAGIC priceBreakdown.basePrice as priceBreakdownbasePrice,
# MAGIC priceBreakdown.baseSubtotal as priceBreakdownbaseSubtotal,
# MAGIC priceBreakdown.clientFee as priceBreakdownclientFee,
# MAGIC priceBreakdown.clientPaidToKiwer as priceBreakdownclientPaidToKiwer,
# MAGIC priceBreakdown.clientSubTotal as priceBreakdownclientSubTotal,
# MAGIC priceBreakdown.clientTax as priceBreakdownclientTax,
# MAGIC priceBreakdown.clientTotal as priceBreakdownclientTotal,
# MAGIC priceBreakdown.commission as priceBreakdowncommission,
# MAGIC priceBreakdown.discountedSubtotal as priceBreakdowndiscountedSubtotal,
# MAGIC priceBreakdown.kiwerFee as priceBreakdownkiwerFee,
# MAGIC priceBreakdown.kiwerPaidAtRestaurant as priceBreakdownkiwerPaidAtRestaurant,
# MAGIC priceBreakdown.kiwiTotalPromo as priceBreakdownkiwiTotalPromo,
# MAGIC priceBreakdown.listingSubtotal as priceBreakdownlistingSubtotal,
# MAGIC priceBreakdown.markup as priceBreakdownmarkup,
# MAGIC priceBreakdown.orderTotal as priceBreakdownorderTotal,
# MAGIC priceBreakdown.orderTotalBeforeDiscounts as priceBreakdownorderTotalBeforeDiscounts,
# MAGIC priceBreakdown.restaurantSubtotal as priceBreakdownrestaurantSubtotal,
# MAGIC priceBreakdown.restaurantTax as priceBreakdownrestaurantTax,
# MAGIC priceBreakdown.restaurantTotal as priceBreakdownrestaurantTotal,
# MAGIC priceBreakdown.restaurantTotalPromo as priceBreakdownrestaurantTotalPromo,
# MAGIC priceBreakdown.subtotal as priceBreakdownsubtotal,
# MAGIC priceBreakdown.thirdPartyTotalPromo as priceBreakdownthirdPartyTotalPromo,
# MAGIC priceBreakdown.tip as priceBreakdowntip,
# MAGIC priceBreakdown.total as priceBreakdowntotal,
# MAGIC priceBreakdown.totalPromo as priceBreakdowntotalPromo,
# MAGIC quickOrder,
# MAGIC rating,
# MAGIC restaurant.name as restaurantName,
# MAGIC restaurant.active as restaurantactive,
# MAGIC restaurant.address as restaurantaddress,
# MAGIC restaurant.basePrice as restaurantbasePrice,
# MAGIC restaurant.commission as restaurantcommission,
# MAGIC restaurant.createdAt as restaurantcreatedAt,
# MAGIC restaurant.deleted as restaurantdeleted,
# MAGIC restaurant.description as restaurantdescription,
# MAGIC restaurant.email as restaurantemail,
# MAGIC restaurant.externalId as restaurantexternalId,
# MAGIC restaurant.externalUrl as restaurantexternalUrl,
# MAGIC restaurant.fee as restaurantfee,
# MAGIC restaurant.homeImage as restauranthomeImage,
# MAGIC restaurant.image as restaurantimage,
# MAGIC restaurant.orderOnlineUrl as restaurantorderOnlineUrl,
# MAGIC restaurant.ordermarkReady as restaurantordermarkReady,
# MAGIC restaurant.ownerFirstName as restaurantownerFirstName,
# MAGIC restaurant.ownerLastName as restaurantownerLastName,
# MAGIC restaurant.paidByKiwer as restaurantpaidByKiwer,
# MAGIC restaurant.partner as restaurantpartner,
# MAGIC restaurant.password as restaurantpassword,
# MAGIC restaurant.phoneNumber as restaurantphoneNumber,
# MAGIC restaurant.priceRange as restaurantpriceRange,
# MAGIC restaurant.priority  as restaurantpriority,
# MAGIC restaurant.promotionalFee as restaurantpromotionalFee,
# MAGIC restaurant.slug as restaurantslug,
# MAGIC restaurant.specialty as restaurantspecialty,
# MAGIC restaurant.tags as restauranttags,
# MAGIC restaurant.tax as restauranttax,
# MAGIC restaurant.zoneId as restaurantzoneId,
# MAGIC restaurant.zoneName as restaurantzoneName,
# MAGIC restaurant.location.city as restaurantlocationCity,
# MAGIC restaurant.location.deleted as restaurantlocationDeleted,
# MAGIC restaurant.location.description as restaurantlocationDescription,
# MAGIC restaurant.location.lat as restaurantLat,
# MAGIC restaurant.location.lng as restaurantlng,
# MAGIC restaurant.location.state as restaurantState,
# MAGIC restaurant.location.zipCode as restaurantZipCode,
# MAGIC restaurantId,
# MAGIC simulated,
# MAGIC state.assignedDate as stateassignedDate,
# MAGIC state.assignedTimestamp as stateassignedTimestamp,
# MAGIC state.cancelled as statecancelled,
# MAGIC state.closeDate as statecloseDate,
# MAGIC state.closeTimestamp as statecloseTimestamp,
# MAGIC state.doorAuthorized as statedoorAuthorized,
# MAGIC state.expired as stateexpired,
# MAGIC state.externalOrderNumber as stateexternalOrderNumber,
# MAGIC state.openDate as stateopenDate,
# MAGIC state.orderMessage as stateorderMessage,
# MAGIC state.pickupComments as statepickupComments,
# MAGIC state.pickupTimestamp as statepickupTimestamp,
# MAGIC state.quotedDeliveryTime as statequotedDeliveryTime,
# MAGIC state.requestStatus as staterequestStatus,
# MAGIC state.restaurantStatus as staterestaurantStatus,
# MAGIC state.reviewStatus as statereviewStatus,
# MAGIC cast(state.openTimestamp as double) as _opentimestamp,
# MAGIC state.statusHistory as statestatusHistory,
# MAGIC state.status as statestatus,
# MAGIC supervisor.active as supervisoractive,
# MAGIC supervisor.degree as supervisordegree,
# MAGIC supervisor.deleted as supervisordeleted,
# MAGIC supervisor.email as supervisoremail,
# MAGIC supervisor.facebookId as supervisorfacebookId,
# MAGIC supervisor.image as supervisorimage,
# MAGIC supervisor.name as supervisorname,
# MAGIC supervisor.password as supervisorpassword,
# MAGIC supervisor.phoneNumber as supervisorphoneNumber,
# MAGIC supervisor.semester as supervisorsemester,
# MAGIC supervisor.state as supervisorstate,
# MAGIC supervisor.zoneId as supervisorzoneId,
# MAGIC supervisorId,
# MAGIC tags,
# MAGIC tip,
# MAGIC zoneId,
# MAGIC CASE
# MAGIC WHEN zone like "%UC BERKELEY%" THEN "Berkeley" 
# MAGIC WHEN zone like "%Denver%" THEN "Denver"
# MAGIC ELSE "Unknow" END AS ops_zone,
# MAGIC CASE
# MAGIC WHEN zone like "%UC BERKELEY%" THEN to_utc_timestamp(from_unixtime((state.openTimestamp/1000)-50000),'America/Los_Angeles')
# MAGIC WHEN zone like "%Denver%" THEN to_utc_timestamp(from_unixtime((state.openTimestamp/1000)-50000),'America/Denver')
# MAGIC ELSE to_utc_timestamp(from_unixtime((state.openTimestamp/1000)-50000),'America/Los_Angeles') END AS date_opentimestamp,
# MAGIC to_date(date_trunc("Month",to_utc_timestamp(from_unixtime((state.openTimestamp/1000)-50000),'America/Los_Angeles')), 'yyyy-MM-dd') as month_partition
# MAGIC FROM __orders

# COMMAND ----------

spark.table("orders").write.partitionBy("month_partition").format("delta").mode("overwrite").save('/mnt/%s/orders/orders_update' % MOUNT_NAME)

# COMMAND ----------

origen = spark.read.format("delta").load('/mnt/%s/orders/orders_origin' % MOUNT_NAME)
origen.createOrReplaceTempView('origin')
update = spark.read.format("delta").load('/mnt/%s/orders/orders_update' % MOUNT_NAME)
update.createOrReplaceTempView('update')

# COMMAND ----------

# MAGIC %sql
# MAGIC MERGE INTO origin
# MAGIC USING update
# MAGIC ON origin.oid = update.oid AND origin.month_partition > date_trunc("Month", current_date()) - INTERVAL 2 month
# MAGIC WHEN MATCHED THEN
# MAGIC   UPDATE SET *
# MAGIC WHEN NOT MATCHED
# MAGIC   THEN INSERT *

# COMMAND ----------

# MAGIC %sql
# MAGIC set spark.databricks.delta.retentionDurationCheck.enabled = false

# COMMAND ----------

spark.sql("VACUUM '/mnt/%s/orders/orders_origin' RETAIN 0 HOURS"  % MOUNT_NAME)
spark.sql("VACUUM '/mnt/%s/orders/orders_update' RETAIN 0 HOURS"  % MOUNT_NAME)

# COMMAND ----------

spark.sql("OPTIMIZE delta.`/mnt/%s/orders/orders_origin`"  % MOUNT_NAME)
spark.sql("GENERATE symlink_format_manifest FOR TABLE delta.`/mnt/%s/orders/orders_origin`"  % MOUNT_NAME)