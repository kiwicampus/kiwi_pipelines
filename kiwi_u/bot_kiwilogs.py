# Databricks notebook source
import os
from datetime import datetime
import pyspark

#Get mongoDB data server ready
source = 'mongodb://%s:%s@ds113633-a1.mlab.com:13633/kiwibot_prod?replicaSet=rs-ds113633' % (os.environ.get("KIWIBOTS_MONGODB_USER"), os.environ.get("KIWIBOTS_MONGODB_PASSWORD"))

#Access s3 bucket
ACCESS_KEY = os.environ.get("s3_o_ACCESS_KEY")
SECRET_KEY = os.environ.get("s3_o_SECRET_KEY")

#Centralize all information in the S3 team bucket
ENCODED_SECRET_KEY = SECRET_KEY.replace("/", "%2F")
AWS_BUCKET_NAME = "centralized-datasets-data-team"
MOUNT_NAME = "centdatasets"
#dbutils.fs.mount("s3a://%s:%s@%s" % (ACCESS_KEY, ENCODED_SECRET_KEY, AWS_BUCKET_NAME), "/mnt/%s" % MOUNT_NAME)

# COMMAND ----------

#Consolidate a one day benchmark to update and append the newest rows to the original database
timestamp = (datetime.timestamp(datetime.now()) - 100000)*1000
timestamp

# COMMAND ----------

def fix_spark_schema(schema):
    """
    Transform mongoDB to Spark dataType schema. This function helps fix mismatch datatypes issues.

    Parameters
    ----------
    schema : structType
        Schema coming from mongoDB structType.

    Returns
    -------
    structType
        New spark schema with a structType datatype.

    """
  if schema.__class__ == pyspark.sql.types.StructType:
    return pyspark.sql.types.StructType([fix_spark_schema(f) for f in schema.fields])
  if schema.__class__ == pyspark.sql.types.StructField:
    return pyspark.sql.types.StructField(schema.name, fix_spark_schema(schema.dataType), schema.nullable)
  if schema.__class__ == pyspark.sql.types.NullType:
    return pyspark.sql.types.StringType()
  return schema

# COMMAND ----------

#Take the latest data from mongoDB and give a structure from conschema.
conschema = spark.read.format("mongo").option("spark.mongodb.input.uri", source).option("database", "kiwibot_prod").option("collection", "kiwerLogs").load().schema
df = spark.read.format("mongo").option("spark.mongodb.input.uri", source).option("database", "kiwibot_prod").option("collection", "kiwerLogs").load(schema=fix_spark_schema(conschema))
df = df.filter(df['timestamp'] >= timestamp)
df.createOrReplaceTempView('__kiwerlogs')

# COMMAND ----------

# MAGIC %sql
# MAGIC CREATE TEMPORARY VIEW _kiwerlogs AS
# MAGIC select 
# MAGIC *,
# MAGIC to_utc_timestamp(from_unixtime(timestamp/1000),'GMT+7') as date_opentimestamp,
# MAGIC to_date(date_trunc("Month",to_utc_timestamp(from_unixtime(timestamp/1000),'GMT+7')), 'yyyy-MM-dd') as month_partition
# MAGIC from __kiwerlogs

# COMMAND ----------

#Save update data to orders_update to s3 database for later use.
spark.table("_kiwerlogs").write.partitionBy("month_partition").format("delta").mode("overwrite").save('/mnt/%s/kiwerlogs/update' % MOUNT_NAME)

# COMMAND ----------

#Read both origen and update database in order to merge them either by updating or apending rows.
origen = spark.read.format("delta").load('/mnt/%s/kiwerlogs/origin' % MOUNT_NAME)
origen.createOrReplaceTempView('originv')
update = spark.read.format("delta").load('/mnt/%s/kiwerlogs/update' % MOUNT_NAME)
update.createOrReplaceTempView('updatev')

# COMMAND ----------

# MAGIC %sql
# MAGIC MERGE INTO originv
# MAGIC USING updatev
# MAGIC ON originv._id.oid = updatev._id.oid AND originv.month_partition > date_trunc("Month", current_date()) - INTERVAL 2 month
# MAGIC WHEN MATCHED THEN
# MAGIC   UPDATE SET *
# MAGIC WHEN NOT MATCHED
# MAGIC   THEN INSERT *

# COMMAND ----------

# MAGIC %sql
# MAGIC set spark.databricks.delta.retentionDurationCheck.enabled = false

# COMMAND ----------

#Read both origen and update database in order to merge them either by updating or apending rows.
spark.sql("VACUUM '/mnt/%s/kiwerlogs/origin' RETAIN 0 HOURS"  % MOUNT_NAME)
spark.sql("VACUUM '/mnt/%s/kiwerlogs/update' RETAIN 0 HOURS"  % MOUNT_NAME)

# COMMAND ----------

#Optimize and generate a symlink_format in order to be able to be read by Athena query engine.
spark.sql("OPTIMIZE delta.`/mnt/%s/kiwerlogs/origin`"  % MOUNT_NAME)
spark.sql("GENERATE symlink_format_manifest FOR TABLE delta.`/mnt/%s/kiwerlogs/origin`"  % MOUNT_NAME)