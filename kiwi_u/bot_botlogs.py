# Databricks notebook source
import os
from datetime import datetime
import pyspark

#Get mongoDB data server ready
source = 'mongodb://%s:%s@ds113633-a1.mlab.com:13633/kiwibot_prod?replicaSet=rs-ds113633' % (os.environ.get("KIWIBOTS_MONGODB_USER"), os.environ.get("KIWIBOTS_MONGODB_PASSWORD"))

#Access s3 bucket
ACCESS_KEY = os.environ.get("s3_o_ACCESS_KEY")
SECRET_KEY = os.environ.get("s3_o_SECRET_KEY")
ENCODED_SECRET_KEY = SECRET_KEY.replace("/", "%2F")

#Centralize all information in the S3 team bucket
AWS_BUCKET_NAME = "centralized-datasets-data-team"
MOUNT_NAME = "centdatasets"
#dbutils.fs.mount("s3a://%s:%s@%s" % (ACCESS_KEY, ENCODED_SECRET_KEY, AWS_BUCKET_NAME), "/mnt/%s" % MOUNT_NAME)

# COMMAND ----------

#Consolidate a one day benchmark to update and append the newest rows to the original database
timestamp = (datetime.timestamp(datetime.now()) - 100000)*1000
timestamp

# COMMAND ----------

def fix_spark_schema(schema):
  """
    Transform mongoDB to Spark dataType schema. This function helps fix mismatch datatypes issues.

    Parameters
    ----------
    schema : structType
        Schema coming from mongoDB structType.

    Returns
    -------
    structType
        New spark schema with a structType datatype.

    """
  if schema.__class__ == pyspark.sql.types.StructType:
    return pyspark.sql.types.StructType([fix_spark_schema(f) for f in schema.fields])
  if schema.__class__ == pyspark.sql.types.StructField:
    return pyspark.sql.types.StructField(schema.name, fix_spark_schema(schema.dataType), schema.nullable)
  if schema.__class__ == pyspark.sql.types.NullType:
    return pyspark.sql.types.StringType()
  return schema

# COMMAND ----------

#Take the latest data from mongoDB and give a structure from conschema.
conschema = spark.read.format("mongo").option("spark.mongodb.input.uri", source).option("database", "kiwibot_prod").option("collection", "botLogs").load().schema
df = spark.read.format("mongo").option("spark.mongodb.input.uri", source).option("database", "kiwibot_prod").option("collection", "botLogs").load(schema=fix_spark_schema(conschema))
df = df.filter(df['timestamp'] >= timestamp)
df.createOrReplaceTempView('__botlogs')

# COMMAND ----------

# MAGIC %sql
# MAGIC CREATE TEMPORARY VIEW __botlogs1 AS
# MAGIC select 
# MAGIC _id.oid as oid,
# MAGIC available,
# MAGIC battery.amps as batteryAmps,
# MAGIC battery.dischargeRate as batteryDischargeRate,
# MAGIC battery.error as batteryError,
# MAGIC battery.percentage as batteryPercentage,
# MAGIC battery.status as batteryStatus,
# MAGIC battery.voltage as batteryVoltage,
# MAGIC clock,
# MAGIC comments,
# MAGIC controller.id_ as controllerId,
# MAGIC controller.userName as controllerUserName,
# MAGIC controller.userEmail as controllerUserEmail,
# MAGIC controller.userLevel as controllerUserLevel,
# MAGIC crossingStreet,
# MAGIC dateStr,
# MAGIC deleted,
# MAGIC event,
# MAGIC gps.accuracy as gpsAccuracy,
# MAGIC gps.heading as gpsHeading,
# MAGIC gps.lat as gpsLat,
# MAGIC gps.lng as gpsLng,
# MAGIC gps.sat as gpsSat,
# MAGIC gps.sensor as gpsSensor,
# MAGIC hardwareVersion,
# MAGIC isCrossingStreet,
# MAGIC last,
# MAGIC lastMaintenance,
# MAGIC lastMaintenanceStatus,
# MAGIC location.accuracy as locationAccuracy,
# MAGIC location.comments as locationComments,
# MAGIC location.dateStr as locationDateStr,
# MAGIC location.description as locationDescription,
# MAGIC location.heading as locationHeading,
# MAGIC location.lat as locationLat,
# MAGIC location.lng as locationLng,
# MAGIC location.precision as locationPrecision,
# MAGIC location.timestamp  as locationTimestamp,
# MAGIC meta.comments as metaComments,
# MAGIC meta.corrections.start as metaCorrectionsStart,
# MAGIC meta.corrections.count as metaCorrectionsCount,
# MAGIC meta.corrections.pilotNetVersion as metaCorrectionsPilotNetVersion,
# MAGIC meta.corrections.end as metaCorrectionsEnd,
# MAGIC meta.corrections.durationMillis as metaCorrectionsDurationMillis,
# MAGIC name,
# MAGIC network.signalStrength4g as networkSignalStrength4g, 
# MAGIC network.snr4g as networkSnr4g,
# MAGIC network.clockDrift as networkClockDrift,
# MAGIC network.controllerAvgLatency as networkControllerAvgLatency,
# MAGIC network.controllerLatency as networkControllerLatency,
# MAGIC network.droppedMsg as networkDroppedMsg,
# MAGIC network.ip as networkIp,
# MAGIC network.sendTime as networkSendTime,
# MAGIC network.serverLatency as networkServerLatency,
# MAGIC network.ssid as networkSsid,
# MAGIC number,
# MAGIC odometry.flipped as odometryFlipped,
# MAGIC odometry.pitch as odometryPitch, 
# MAGIC odometry.roll as odometryRoll,
# MAGIC odometry.vx as odometryVx,
# MAGIC odometry.yaw as odometryYaw,
# MAGIC odometry.yaw_speed as odometryYaw_speed,
# MAGIC online,
# MAGIC role,
# MAGIC state,
# MAGIC status.capture as statusCapture,
# MAGIC status.door as statusDoor,
# MAGIC status.gear as statusGear,
# MAGIC status.message as statusMessage,
# MAGIC status.mode as statusMode,
# MAGIC status.skynet as statusSkynet,
# MAGIC status.space_left as statusSpace_left,
# MAGIC timestamp,
# MAGIC type,
# MAGIC to_utc_timestamp(from_unixtime(timestamp/1000),'GMT+7') as date_opentimestamp,
# MAGIC to_date(date_trunc("Month",to_utc_timestamp(from_unixtime(timestamp/1000),'GMT+7')), 'yyyy-MM-dd') as month_partition
# MAGIC from __botlogs

# COMMAND ----------

#Save update data to orders_update to s3 database for later use.
spark.table("__botlogs1").write.partitionBy("month_partition").format("delta").mode("overwrite").save('/mnt/%s/botLogs/botLogsupdate' % MOUNT_NAME)

# COMMAND ----------

#Read both origen and update database in order to merge them either by updating or apending rows.
origen = spark.read.format("delta").load('/mnt/%s/botLogs/botLogsorigin' % MOUNT_NAME)
origen.createOrReplaceTempView('originv')
update = spark.read.format("delta").load('/mnt/%s/botLogs/botLogsupdate' % MOUNT_NAME)
update.createOrReplaceTempView('updatev')

# COMMAND ----------

# MAGIC %sql
# MAGIC MERGE INTO originv
# MAGIC USING updatev
# MAGIC ON originv.oid = updatev.oid AND originv.month_partition > date_trunc("Month", current_date()) - INTERVAL 2 month
# MAGIC WHEN MATCHED THEN
# MAGIC   UPDATE SET *
# MAGIC WHEN NOT MATCHED
# MAGIC   THEN INSERT *

# COMMAND ----------

# MAGIC %sql
# MAGIC set spark.databricks.delta.retentionDurationCheck.enabled = false

# COMMAND ----------

#Do not save older versions of orders_origin and orders_update to not save unnecesary data. 
spark.sql("VACUUM '/mnt/%s/botLogs/botLogsorigin' RETAIN 0 HOURS"  % MOUNT_NAME)
spark.sql("VACUUM '/mnt/%s/botLogs/botLogsupdate' RETAIN 0 HOURS"  % MOUNT_NAME)

# COMMAND ----------

#Optimize and generate a symlink_format in order to be able to be read by Athena query engine.
spark.sql("OPTIMIZE delta.`/mnt/%s/botLogs/botLogsorigin`"  % MOUNT_NAME)
spark.sql("GENERATE symlink_format_manifest FOR TABLE delta.`/mnt/%s/botLogs/botLogsorigin`"  % MOUNT_NAME)