## Kiwi´s Data Analytics Pipelines

This repository stores all the notebooks´ pipelines that make possible setting and updating Kiwi´s databases.
AWS is used as a storage, query engine (Athena) and clusters sevice for these notebooks as cloud provider. On the other hand,
Databricks is set to be the notebook's runtime provider working on the EC2 clusters. All of this, ensure that analytics
could be done inside the start-up.

---

## Background

Before the implementation of these pipelines, Kiwi used an engine pipeline provider call Stitch, which it connects to a Bigquery query database. The main issue around this architecture was that many rows couldn't be updated. There
were many communications with the cloud provider but unfortunately there´s nothing to be done with an immutable database.
This implies that databases must be manageable and flexible for people to be able to used them.

---


## Data Model Structure
Given that data comes in different shapes as no-sql data, its small compare to bigger schemes and there´s data errors in some rows. For this reason, the structure right now implemented is a data lake.

- **What´s the Data Lake advantages?**

    **Storage**: Designed for low cost storage. Tools: AWS s3 and Blob Storage Azure.

    **Processing**: Flexible clusters or on demand procees. Tools: Databricks and Athena AWS.

    **Agility**: As databases are not centralized, its faster to get data to production.
    So, highly agile configure and reconfigure as needed.

- **Datawarehouse comparison:**

    **Storage**: Expensive for large datasets. ($0.25 for 0.16TB SSD per hour/node dc2.large**).

    **Processing**: Process time is attached to storage pointing out waste resources in either feature if its decided to increase the first or second.

    **Agility**: As databases have to be normalized, it needs more time to be implemented.

Given these reasons, the following chart explains this repository data workflow:

![Data Workflow](data_workflow.png)

As shown in the data-workflow:

1. Data sources comes from many APIs and mongoDB.
2. Then, its process through the pyspark data pipelines that arrives to a S3 bucket stored as a delta lake file.
3. Finally, Athena comes as a query engine in order to be connected to the BI tool, in this case Mode.

## Recommendations

- If the centralized data surpass 10 Gb of storage, it should be consider to integrate ElasticSearch and Kibana.
- For partition data in Athena, it should be update the partitions on the Athena console at the beginning of each month.
- If pipelines are hard to handle, its better to implement Airflow to manage this system.