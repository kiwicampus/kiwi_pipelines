# Databricks notebook source
import os
import pyspark

#Get mongoDB data server ready
source = 'mongodb://%s:%s@ds347758-a0.mlab.com:47758,ds347758-a1.mlab.com:47758/kiwi-maintenance?replicaSet=rs-ds347758' % ("datateam", "datateam2019")

#Access s3 bucket
ACCESS_KEY = os.environ.get("s3_o_ACCESS_KEY")
SECRET_KEY = os.environ.get("s3_o_SECRET_KEY")

#Centralize all information in the S3 team bucket
ENCODED_SECRET_KEY = SECRET_KEY.replace("/", "%2F")
AWS_BUCKET_NAME = "centralized-datasets-data-team"
MOUNT_NAME = "centdatasets"

#dbutils.fs.mount("s3a://%s:%s@%s" % (ACCESS_KEY, ENCODED_SECRET_KEY, AWS_BUCKET_NAME), "/mnt/%s" % MOUNT_NAME)

# COMMAND ----------

def fix_spark_schema(schema):
  """
    Transform mongoDB to Spark dataType schema. This function helps fix mismatch datatypes issues.

    Parameters
    ----------
    schema : structType
        Schema coming from mongoDB structType.

    Returns
    -------
    structType
        New spark schema with a structType datatype.

    """
  if schema.__class__ == pyspark.sql.types.StructType:
    return pyspark.sql.types.StructType([fix_spark_schema(f) for f in schema.fields])
  if schema.__class__ == pyspark.sql.types.StructField:
    return pyspark.sql.types.StructField(schema.name, fix_spark_schema(schema.dataType), schema.nullable)
  if schema.__class__ == pyspark.sql.types.NullType:
    return pyspark.sql.types.StringType()
  return schema

# COMMAND ----------

#Take the latest data from mongoDB and give a structure from conschema.
conschema = spark.read.format("mongo").option("spark.mongodb.input.uri", source).option("database", "kiwi-maintenance").option("collection", "reports-logs").load().schema
df = spark.read.format("mongo").option("spark.mongodb.input.uri", source).option("database", "kiwi-maintenance").option("collection", "reports-logs").load(schema=fix_spark_schema(conschema))
df.createOrReplaceTempView('manteinance')

# COMMAND ----------

#Save update data to orders_update to s3 database for later use.
spark.table("manteinance").write.format("delta").option("mergeSchema", "true").mode("overwrite").save('/mnt/%s/mantainance/reports-logs' % MOUNT_NAME)

# COMMAND ----------

# MAGIC %sql
# MAGIC set spark.databricks.delta.retentionDurationCheck.enabled = false

# COMMAND ----------

#Read both origen and update database in order to merge them either by updating or apending rows and optimize and generate a symlink_format in order to be able to be read by Athena query engine.
spark.sql("VACUUM '/mnt/%s/mantainance/reports-logs' RETAIN 0 HOURS"  % MOUNT_NAME)
spark.sql("OPTIMIZE delta.`/mnt/%s/mantainance/reports-logs`"  % MOUNT_NAME)
spark.sql("GENERATE symlink_format_manifest FOR TABLE delta.`/mnt/%s/mantainance/reports-logs`"  % MOUNT_NAME)