# Databricks notebook source
import os
import json
from intercom.client import Client
import base64
import requests

#Get intercom user request ready 
intercom = Client(personal_access_token=os.environ.get("intercom_token"))

#Access s3 bucket
ACCESS_KEY = os.environ.get("s3_o_ACCESS_KEY")
SECRET_KEY = os.environ.get("s3_o_SECRET_KEY")
ENCODED_SECRET_KEY = SECRET_KEY.replace("/", "%2F")

#Centralize all information in the S3 team bucket
AWS_BUCKET_NAME = "centralized-datasets-data-team"
MOUNT_NAME = "centdatasets"

#dbutils.fs.mount("s3a://%s:%s@%s" % (ACCESS_KEY, ENCODED_SECRET_KEY, AWS_BUCKET_NAME), "/mnt/%s" % MOUNT_NAME)

# COMMAND ----------

#Access to GET users from headline
auth = base64.b64encode(str.encode(os.environ.get("intercom_token"))).decode("ascii")
headers = {'Authorization': 'Basic {encoded_secret}'.format(encoded_secret=auth), 'Accept': "application/json"}
url = "https://api.intercom.io/users"

#Query the 40 users from the latest data
querystring = {"page": 1, "per_page": 40, "order": "desc"}
response = requests.get(url, headers=headers, params = querystring)
response_json = response.json()

#Save a json in order to parse it later on parquets
with open("/dbfs/foobar/temp_users_up.json", 'w') as f:
      json.dump(response_json['users'], f)
table1 = spark.read.format("json").load("foobar/temp_users_up.json")
table2 = table1.select("id")
table2.write.format("parquet").mode("overwrite").save('/mnt/%s/intercom/users/update_list' % MOUNT_NAME)

# COMMAND ----------

#Create a schema with unique users id
user_list_clear = spark.read.format("parquet").load('/mnt/%s/intercom/users/update_list' % MOUNT_NAME)
user_list_clear = user_list_clear.select("id").distinct()

# COMMAND ----------

def take_users(user_id):
  """
    Fetch each user into the headline in order to get personal information. Then, append this data to a temporary database.

    Parameters
    ----------
    user_id : string
        Id that represents an user.

    Returns
    -------
    user_id : string
        Id that represents an user.
    """
  url = "https://api.intercom.io/users/%s" % user_id
  response = requests.get(url, headers=headers)
  response_json = response.json()
  
  with open("/dbfs/foobar/temp_response.json", 'w') as f:
      json.dump(response_json, f)
    
  onschema = spark.read.format("delta").load('/mnt/%s/intercom/users/users_origin' % MOUNT_NAME).schema
  table = spark.read.format("json").load("foobar/temp_response.json", schema=onschema)
  table.write.format("delta").option("mergeSchema", "true").mode("append").save('/mnt/%s/intercom/users/users_updatev1' % MOUNT_NAME)
  return user_id

# COMMAND ----------

#Make a look for an entire user list.
n=1
while True:
  try:
    row = user_list_clear.collect()[n]
    r = row.id
    print(r, n)
    take_users(r)
    n=n+1
  except: break

# COMMAND ----------

#Take origin and update tables in order to merge them

origin = spark.read.format("delta").load('/mnt/%s/intercom/users/users_origin' % MOUNT_NAME)
origin.createOrReplaceTempView('originv')
update= spark.read.format("delta").load('/mnt/%s/intercom/users/users_updatev1' % MOUNT_NAME)
update.createOrReplaceTempView('updatev')

# COMMAND ----------

# MAGIC %sql
# MAGIC MERGE INTO originv
# MAGIC USING updatev
# MAGIC ON originv.id = updatev.id
# MAGIC WHEN MATCHED THEN
# MAGIC   UPDATE SET *
# MAGIC WHEN NOT MATCHED
# MAGIC   THEN INSERT *

# COMMAND ----------

# MAGIC %sql
# MAGIC set spark.databricks.delta.retentionDurationCheck.enabled = false

# COMMAND ----------

#Write flat table to a delta lake named orders_origin partition by month_partition into the S3 bucket.
spark.sql("VACUUM '/mnt/%s/intercom/users/users_origin' RETAIN 0 HOURS"  % MOUNT_NAME)
spark.sql("VACUUM '/mnt/%s/intercom/users/users_updatev1' RETAIN 0 HOURS"  % MOUNT_NAME)

# COMMAND ----------

#Optimize and generate a symlink_format in order to be able to be read by Athena query engine.
spark.sql("OPTIMIZE delta.`/mnt/%s/intercom/users/users_origin`"  % MOUNT_NAME)
spark.sql("GENERATE symlink_format_manifest FOR TABLE delta.`/mnt/%s/intercom/users/users_origin`" % MOUNT_NAME)