# Databricks notebook source
import os
import json
from intercom.client import Client
import base64
import requests


#Get intercom user request ready 
intercom = Client(personal_access_token=os.environ.get("intercom_token"))

#Access s3 bucket
ACCESS_KEY = os.environ.get("s3_o_ACCESS_KEY")
SECRET_KEY = os.environ.get("s3_o_SECRET_KEY")

#Centralize all information in the S3 team bucket
ENCODED_SECRET_KEY = SECRET_KEY.replace("/", "%2F")
AWS_BUCKET_NAME = "centralized-datasets-data-team"
MOUNT_NAME = "centdatasets"

#dbutils.fs.mount("s3a://%s:%s@%s" % (ACCESS_KEY, ENCODED_SECRET_KEY, AWS_BUCKET_NAME), "/mnt/%s" % MOUNT_NAME)

# COMMAND ----------

#Access to GET segments from headline
auth = base64.b64encode(str.encode(os.environ.get("intercom_token"))).decode("ascii")
headers = {'Authorization': 'Basic {encoded_secret}'.format(encoded_secret=auth), 'Accept': "application/json"}
url = "https://api.intercom.io/segments"

#Query segments from the latest data
response = requests.get(url, headers=headers)
response_json = response.json()
with open("/dbfs/foobar/temp.json", 'w') as f:
  json.dump(response_json['segments'], f)

#Save a json in order to parse it later on parquets
table1 = spark.read.format("json").load("foobar/temp.json")
table1.write.format("delta").mode("overwrite").save('/mnt/%s/intercom/segments' % MOUNT_NAME)

# COMMAND ----------

#Access to GET tags from headline
url = "https://api.intercom.io/tags"

#Query segments from the latest data
response = requests.get(url, headers=headers)
response_json = response.json()
with open("/dbfs/foobar/temp.json", 'w') as f:
  json.dump(response_json['tags'], f)

#Save a json in order to parse it later on parquets
table1 = spark.read.format("json").load("foobar/temp.json")
table1.write.format("delta").mode("overwrite").save('/mnt/%s/intercom/tags' % MOUNT_NAME)

# COMMAND ----------

# MAGIC %sql
# MAGIC set spark.databricks.delta.retentionDurationCheck.enabled = false

# COMMAND ----------

#Write flat table to a delta lake named orders_origin partition by month_partition into the S3 bucket.
spark.sql("VACUUM '/mnt/%s/intercom/segments' RETAIN 0 HOURS"  % MOUNT_NAME)
spark.sql("VACUUM '/mnt/%s/intercom/tags' RETAIN 0 HOURS"  % MOUNT_NAME)

# COMMAND ----------

#Optimize and generate a symlink_format in order to be able to be read by Athena query engine.
spark.sql("OPTIMIZE delta.`/mnt/%s/intercom/segments`"  % MOUNT_NAME)
spark.sql("GENERATE symlink_format_manifest FOR TABLE delta.`/mnt/%s/intercom/segments`" % MOUNT_NAME)
spark.sql("OPTIMIZE delta.`/mnt/%s/intercom/tags`"  % MOUNT_NAME)
spark.sql("GENERATE symlink_format_manifest FOR TABLE delta.`/mnt/%s/intercom/tags`" % MOUNT_NAME)