# Databricks notebook source
import os
import json
from intercom.client import Client
import base64
import requests

#Get intercom user request ready 
intercom = Client(personal_access_token=os.environ.get("intercom_token"))

#Access s3 bucket
ACCESS_KEY = os.environ.get("s3_o_ACCESS_KEY")
SECRET_KEY = os.environ.get("s3_o_SECRET_KEY")

#Centralize all information in the S3 team bucket
ENCODED_SECRET_KEY = SECRET_KEY.replace("/", "%2F")
AWS_BUCKET_NAME = "centralized-datasets-data-team"
MOUNT_NAME = "centdatasets"

#dbutils.fs.mount("s3a://%s:%s@%s" % (ACCESS_KEY, ENCODED_SECRET_KEY, AWS_BUCKET_NAME), "/mnt/%s" % MOUNT_NAME)

# COMMAND ----------

update = spark.read.format("delta").load('/mnt/%s/intercom/conversations/backup_conversations/1' % MOUNT_NAME)
update.write.format("delta").option("mergeSchema", "true").mode("append").save('/mnt/%s/intercom/conversations/origin_conversations' % MOUNT_NAME)

# COMMAND ----------

auth = base64.b64encode(str.encode(os.environ.get("intercom_token"))).decode("ascii")
headers = {'Authorization': 'Basic {encoded_secret}'.format(encoded_secret=auth), 'Accept': "application/json"}
url = "https://api.intercom.io/conversations"

querystring = {"page": 1, "per_page": 20, "order": "desc"}
response = requests.get(url, headers=headers, params = querystring)
response_json = response.json()

with open("/dbfs/foobar/temp_users_up.json", 'w') as f:
      json.dump(response_json['users'], f)
table1 = spark.read.format("json").load("foobar/temp_users_up.json")
table2 = table1.select("id")
table2.write.format("parquet").mode("overwrite").save('/mnt/%s/intercom/users/update_list' % MOUNT_NAME)

# COMMAND ----------

querystring = {"page": 1, "per_page": 60, "order": "desc"}

 
response = requests.get(url, headers=headers, params=querystring)
response_json = response.json()
with open("/dbfs/foobar/test3.json", 'w') as f:
  json.dump(response_json['conversations'], f)

table1 = spark.read.format("json").load("foobar/test3.json")
table1.createOrReplaceTempView('tablea')
table2 = sql("select assignee.id as assigneeId, assignee.type as assigneeType, conversation_message.author.id as conversation_messageAuthorId, conversation_message.author.type as conversation_messageAuthortype, conversation_message.id as conversation_messageId, conversation_message.subject as conversation_messageSubject, conversation_message.type as conversation_messageType, conversation_message.url as conversation_messageUrl, created_at, customers.id as customersId,customers.type as customerstype, id, open, read, snoozed_until, state, type,updated_at, user.id as userId, user.type as userType, waiting_since, to_utc_timestamp(from_unixtime(created_at),'GMT+7') as date_opentimestamp, to_date(date_trunc('Month',to_utc_timestamp(from_unixtime(created_at),'GMT+7')), 'yyyy-MM-dd') as month_partition from tablea")
table2.write.format("delta").mode("overwrite").save('/mnt/%s/intercom/conversationsUpdate' % MOUNT_NAME)

# COMMAND ----------

up = spark.read.format("delta").load('/mnt/%s/intercom/conversationsv2' % MOUNT_NAME)
up.createOrReplaceTempView('originv')
do = spark.read.format("delta").load('/mnt/%s/intercom/conversationsUpdate' % MOUNT_NAME)
do.createOrReplaceTempView('updatev')

# COMMAND ----------

# MAGIC %sql
# MAGIC MERGE INTO originv
# MAGIC USING updatev
# MAGIC ON originv.id = updatev.id AND originv.month_partition > date_trunc("Month", current_date()) - INTERVAL 2 month
# MAGIC WHEN MATCHED THEN
# MAGIC   UPDATE SET *
# MAGIC WHEN NOT MATCHED
# MAGIC   THEN INSERT *

# COMMAND ----------

# MAGIC %sql
# MAGIC set spark.databricks.delta.retentionDurationCheck.enabled = false

# COMMAND ----------

spark.sql("VACUUM '/mnt/%s/intercom/conversationsUpdate' RETAIN 0 HOURS"  % MOUNT_NAME)
spark.sql("VACUUM '/mnt/%s/intercom/conversationsv2' RETAIN 0 HOURS"  % MOUNT_NAME)

# COMMAND ----------

#spark.sql("OPTIMIZE delta.`/mnt/%s/intercom/conversations/origin_conversations`"  % MOUNT_NAME)
spark.sql("GENERATE symlink_format_manifest FOR TABLE delta.`/mnt/%s/intercom/conversations/origin_conversations`" % MOUNT_NAME)